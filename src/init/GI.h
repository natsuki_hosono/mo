#define SELF_GRAVITY
#define FLAG_GI
#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
#error
#endif

template <class Ptcl> class GI_init : public Problem<Ptcl>{
	public:
	//Normal
	static const PS::F64 UnitMass = 6.0e+24 * 1.0; // Earth mass
	//MO
	static const PS::F64 UnitRadi = 6400e+3 * 1.175; // Earth radii
	//full solid
	//static const PS::F64 UnitRadi = 6400e+3 * 0.75; // Earth radii
	static const PS::F64 Grav = 6.67e-11;
	
	static PS::F64 UnitTime(){
		return sqrt(UnitRadi * UnitRadi * UnitRadi / (Grav * UnitMass));
	}
	static PS::F64 DynamicalTime(){
		return math::pi * UnitTime();
	}
	static PS::F64 EscapeVelocity(){
		return sqrt(2.0 * Grav * UnitMass / UnitRadi);
	}
	static const double END_TIME = 2.0e+4;

	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		const int putImpactor = 0;
		const PS::F64 dx = 1.0 / 42.0;
		//const PS::F64 dx = 1.0 / 17.0;
		//std::cin >> putImpactor;

		std::cout << "putImpactor = " << putImpactor << std::endl;
		/////////
		//place ptcls
		/////////
		std::vector<Ptcl> ptcl;
		std::vector<Ptcl> tar;//Target
		std::vector<Ptcl> imp;//Impactor
		/////////
		/////////
		const PS::F64 Expand = 0.9;
		const PS::F64 tarMass = UnitMass;
		const PS::F64 tarRadi = UnitRadi;
		const PS::F64 tarCoreMass = 0.3 * tarMass;
		const PS::F64 tarCoreRadi = cbrt(tarCoreMass / tarMass) * tarRadi;
		//const PS::F64 tarSolidRadi = 0.8 * tarRadi;//Solid radius
		//const PS::F64 tarSolidRadi = 0.85 * tarRadi;//Solid radius
		//const PS::F64 tarSolidRadi = 0.9 * tarRadi;//Solid radius
		//const PS::F64 tarSolidRadi = 0.95 * tarRadi;//Solid radius
		const PS::F64 tarSolidRadi = 1.0 * tarRadi;//Solid radius

		const PS::F64 impMass = 0.1 * tarMass;
		const PS::F64 impRadi = Expand * cbrt(impMass / tarMass) * UnitRadi;
		const PS::F64 impCoreMass = 0.3 * impMass;
		const PS::F64 impCoreRadi = cbrt(impCoreMass / impMass) * impRadi;

		const double offset = 0.0 * UnitRadi;
		///////////////////
		//Dummy put to determine # of ptcls
		///////////////////
		//target
		int tarNmntl = 0;
		for(PS::F64 x = -1.0 ; x <= 1.0 ; x += dx){
			for(PS::F64 y = -1.0 ; y <= 1.0 ; y += dx){
				for(PS::F64 z = -1.0 ; z <= 1.0 ; z += dx){
					const PS::F64 r = sqrt(x*x + y*y + z*z) * UnitRadi;
					if(r >= tarRadi) continue;
					++ tarNmntl;
				}
			}
		}
		//imp
		int impNmntl = 0;
		for(PS::F64 x = -1.0 ; x <= 1.0 ; x += dx){
			for(PS::F64 y = -1.0 ; y <= 1.0 ; y += dx){
				for(PS::F64 z = -1.0 ; z <= 1.0 ; z += dx){
					const PS::F64 r = Expand * sqrt(x*x + y*y + z*z) * UnitRadi;
					if(r >= impRadi) continue;
					++ impNmntl;
				}
			}
		}
		///////////////////
		//Dummy end
		///////////////////
		const int tarNptcl = tarNmntl;
		const int impNptcl = impNmntl;
		const int Nptcl    = tarNptcl + impNptcl;
		std::cout << "Target  :" << tarNptcl << std::endl;
		std::cout << "    radius           : " << tarRadi << std::endl;
		std::cout << "    # of mantle ptcls: " << tarNmntl << std::endl;
		std::cout << "    mantle density   : " << (tarMass) / (4.0 * math::pi / 3.0 * (tarRadi * tarRadi * tarRadi)) << std::endl;
		std::cout << "    core radius      : " << tarCoreRadi << std::endl;
		std::cout << "Impactor:" << impNptcl << std::endl;
		std::cout << "    radius           : " << impRadi << std::endl;
		std::cout << "    # of mantle ptcls: " << impNmntl << std::endl;
		std::cout << "    mantle density   : " << (impMass) / (4.0 * math::pi / 3.0 * (impRadi * impRadi * impRadi)) << std::endl;
		std::cout << "Total:" << Nptcl << std::endl;
		std::cout << "Tar-to-Imp mass ratio: " << (double)(impNmntl) / (double)(tarNmntl) << std::endl;
		const int NptclIn1Node = Nptcl / PS::Comm::getNumberOfProc();
		///////////////////
		//Real put
		///////////////////
		PS::S32 id = 0;
		//Put Tar.
		for(PS::F64 x = -1.0 ; x <= 1.0 ; x += dx){
			for(PS::F64 y = -1.0 ; y <= 1.0 ; y += dx){
				for(PS::F64 z = -1.0 ; z <= 1.0 ; z += dx){
					const PS::F64 r = sqrt(x*x + y*y + z*z) * UnitRadi;
					if(r >= tarRadi) continue;
					Ptcl ith;
					ith.pos.x = UnitRadi * x;
					ith.pos.y = UnitRadi * y;
					ith.pos.z = UnitRadi * z;
					ith.dens = (tarMass) / (4.0 / 3.0 * math::pi * (tarRadi * tarRadi * tarRadi));
					ith.mass = tarMass + impMass;
					ith.id   = id++;
					//ith.tag = (sqrt(ith.pos * ith.pos) < tarCoreRadi) ? 1 : 0;
					if(sqrt(ith.pos * ith.pos) < tarCoreRadi){
						//CORE
						ith.tag = 0;
						//ith.eng  = 0.005 * Grav * tarMass / tarRadi;
						ith.eng = Iron.getSpecificThermalEnergyViaDensityAndTemperature(ith.dens, 4000.0);
					}else if(sqrt(ith.pos * ith.pos) <= tarSolidRadi){
						//MNTL SOL
						ith.tag = 1;
						//ith.eng  = 0.05 * Grav * tarMass / tarRadi;
						ith.eng = Dunite.getSpecificThermalEnergyViaDensityAndTemperature(ith.dens, 4000.0);
					}else{
						//MNTL MO
						ith.tag = 2;
						//ith.eng  = 0.005 * Grav * tarMass / tarRadi;
						ith.eng = Olivine.getSpecificThermalEnergyViaDensityAndTemperature(ith.dens, 4000.0);
					}
					if(ith.id / NptclIn1Node == PS::Comm::getRank()) tar.push_back(ith);
				}
			}
		}
		//imp
		for(PS::F64 x = -1.0 ; x <= 1.0 ; x += dx){
			for(PS::F64 y = -1.0 ; y <= 1.0 ; y += dx){
				for(PS::F64 z = -1.0 ; z <= 1.0 ; z += dx){
					const PS::F64 r = Expand * sqrt(x*x + y*y + z*z) * UnitRadi;
					if(r >= impRadi) continue;
					Ptcl ith;
					ith.pos.x = Expand * UnitRadi * x;
					ith.pos.y = Expand * UnitRadi * y;
					ith.pos.z = Expand * UnitRadi * z;
					ith.dens = (impMass) / (4.0 / 3.0 * math::pi * (impRadi * impRadi * impRadi));
					ith.mass = tarMass + impMass;
					ith.eng  = 0.03 * Grav * impMass / impRadi;
					ith.id   = id++;
					ith.tag = (sqrt(ith.pos * ith.pos) < impCoreRadi) ? 3 : 4;
					ith.pos.x += offset;
					if(ith.id / NptclIn1Node == PS::Comm::getRank()) imp.push_back(ith);
				}
			}
		}
		for(PS::U32 i = 0 ; i < tar.size() ; ++ i){
			tar[i].mass /= (PS::F64)(Nptcl);
		}
		for(PS::U32 i = 0 ; i < imp.size() ; ++ i){
			imp[i].mass /= (PS::F64)(Nptcl);
		}

		if(putImpactor == 0){
			for(PS::U32 i = 0 ; i < tar.size() ; ++ i){
				ptcl.push_back(tar[i]);
			}
		}else{
			for(PS::U32 i = 0 ; i < imp.size() ; ++ i){
				ptcl.push_back(imp[i]);
			}
		}
		const PS::S32 numPtclLocal = ptcl.size();
		sph_system.setNumberOfParticleLocal(numPtclLocal);
		for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
			sph_system[i] = ptcl[i];
			sph_system[i].initialize();
		}
		//Fin.
		std::cout << "# of ptcls = " << ptcl.size() << std::endl;

		dinfo.decomposeDomainAll(sph_system);
		sph_system.exchangeParticle(dinfo);
		namespace PTCL = STD;
		PS::TreeForForceShort<PTCL::RESULT::Dens , PTCL::EPI::Dens , PTCL::EPJ::Dens >::Gather   dens_tree;
		dens_tree.initialize(sph_system.getNumberOfParticleLocal(), 0.5, 1, 1);
		dens_tree.calcForceAllAndWriteBack(PTCL::CalcDensity(), sph_system, dinfo);
		#pragma omp parallel for
		for(PS::S32 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			sph_system[i].eng *= sph_system[i].dens / ((tarMass) / (4.0 * math::pi / 3.0 * (tarRadi * tarRadi * tarRadi)));
		}
		std::cout << "setup..." << std::endl;
	}
	static void setEoS(PS::ParticleSystem<Ptcl>& sph_system){
		#pragma omp parallel for
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			if(sph_system[i].tag == 0 || sph_system[i].tag == 3){
				sph_system[i].setPressure(&Iron);
			}else if(sph_system[i].tag == 2){
				sph_system[i].setPressure(&Olivine);
			}else if(sph_system[i].tag == 1){
				sph_system[i].setPressure(&Dunite);
			}else{
				sph_system[i].setPressure(&Dunite);
				//sph_system[i].setPressure(&Basalt);
			}
		}
	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo){
		if(sysinfo.time < 5000.0){
			std::cout << "Add Frictional Force." << std::endl;
			#pragma omp parallel for
			for(PS::S32 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
				sph_system[i].acc += - sph_system[i].vel * 0.1 / sph_system[i].dt;
			}
		}else if(sysinfo.time < 10000.0){
			std::cout << "Add Frictional Force." << std::endl;
			#pragma omp parallel for
			for(PS::S32 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
				sph_system[i].acc += - sph_system[i].vel * 0.05 / sph_system[i].dt;
			}
		}else{
			#if 0
			#pragma omp parallel for
			for(PS::S32 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
				sph_system[i].acc += - sph_system[i].vel * 0.1 / sph_system[i].dt;
			}
			static bool isFirst = true;
			if(isFirst){
				std::cout << "eng" << std::endl;
				#pragma omp parallel for
				for(PS::S32 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
					const double r = sqrt(sph_system[i].pos * sph_system[i].pos);
					if(sph_system[i].tag == 1){
						sph_system[i].eng = sph_system[i].eng_half = Dunite.getSpecificThermalEnergyViaDensityAndTemperature(sph_system[i].dens, 2000.0 * sqrt(-(r / 7.0e+6 - 1.0)) + 1500.0);
					}else if(sph_system[i].tag == 0){
						sph_system[i].eng = sph_system[i].eng_half = Iron.getSpecificThermalEnergyViaDensityAndTemperature(sph_system[i].dens, 4000.0 * sqrt(-(r / 7.0e+6 - 1.0)) + 1500.0);
					}else{
						sph_system[i].eng = sph_system[i].eng_half = Olivine.getSpecificThermalEnergyViaDensityAndTemperature(sph_system[i].dens, 2000.0 * sqrt(-(r / 7.0e+6 - 1.0)) + 1500.0);
					}
					//if(isnan(sph_system[i].eng)) std::cout << "ERROR" << std::endl;
				}
				isFirst = false;
			}
			#endif
		}
	}
};

template <class Ptcl> class GI_init2 : public Problem<Ptcl>{
	public:
	static const double END_TIME = 10000.0;
	static const PS::F64 R = 6400.0e+3;
	static const double Grav = 6.67e-11;

	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		FileHeader header;
		char filename[256];
		sprintf(filename, "init");
		sph_system.readParticleAscii(filename, "./%s_%05d_%05d.dat", header);
	}
	static void setEoS(PS::ParticleSystem<Ptcl>& sph_system){
		#pragma omp parallel for
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			if(sph_system[i].tag == 0 || sph_system[i].tag == 3){
				sph_system[i].setPressure(&Iron);
			}else if(sph_system[i].tag == 2){
				sph_system[i].setPressure(&Olivine);
			}else if(sph_system[i].tag == 1){
				sph_system[i].setPressure(&Dunite);
			}else{
				sph_system[i].setPressure(&Dunite);
				//sph_system[i].setPressure(&Basalt);
			}
		}
	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo){
		if(sysinfo.time < 5000.0){
			std::cout << "Add Frictional Force." << std::endl;
			#pragma omp parallel for
			for(PS::S32 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
				sph_system[i].acc += - sph_system[i].vel * 0.1 / sph_system[i].dt;
			}
		}
	}
};

template <class Ptcl> class GI : public Problem<Ptcl>{
	public:
	static const double END_TIME = 100000.0 * 10.0;
	static const PS::F64 R = 6400.0e+3;
	static const PS::F64 M = 6.0e+24;
	static const double Grav = 6.67e-11;
	static const double L_EM = 3.5e+34;
	//static const PS::F64 AngMomEarthSpin = 7.1e+33;???
	static const PS::F64 InertiaEarth    = 8.1e+37;

	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		FileHeader header;
		char filename[256];
		sprintf(filename, "init");
		sph_system.readParticleAscii(filename, "./%s_%05d_%05d.dat", header);
		//Shift positions
		std::vector<Ptcl> tar, imp;
		PS::F64vec pos_tar = 0;
		PS::F64vec pos_imp = 0;
		PS::F64vec vel_imp = 0;
		PS::F64 mass_tar = 0;
		PS::F64 mass_imp = 0;
		PS::F64 radi_tar = 0;
		PS::F64 radi_imp = 0;
		for(PS::U32 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			if(sph_system[i].tag <= 2){
				//target
				pos_tar  += sph_system[i].mass * sph_system[i].pos;
				mass_tar += sph_system[i].mass;
			}else{
				//impactor
				pos_imp  += sph_system[i].mass * sph_system[i].pos;
				vel_imp  += sph_system[i].mass * sph_system[i].vel;
				mass_imp += sph_system[i].mass;
			}
		}
		//accumurate
		pos_tar  = PS::Comm::getSum( pos_tar);
		pos_imp  = PS::Comm::getSum( pos_imp);
		vel_imp  = PS::Comm::getSum( vel_imp);
		mass_tar = PS::Comm::getSum(mass_tar);
		mass_imp = PS::Comm::getSum(mass_imp);
		pos_tar /= mass_tar;
		pos_imp /= mass_imp;
		vel_imp /= mass_imp;
		for(PS::U32 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			if(sph_system[i].tag <= 2){
				//target
				radi_tar = std::max(radi_tar, sqrt((pos_tar - sph_system[i].pos) * (pos_tar - sph_system[i].pos)));
			}else{
				//impactor
				radi_imp = std::max(radi_imp, sqrt((pos_imp - sph_system[i].pos) * (pos_imp - sph_system[i].pos)));
			}
		}
		radi_tar = PS::Comm::getMaxValue(radi_tar);
		radi_imp = PS::Comm::getMaxValue(radi_imp);
		std::cout << radi_tar << std::endl;
		std::cout << radi_imp << std::endl;

		const double v_esc = sqrt(2.0 * Grav * (mass_tar + mass_imp) / (radi_tar + radi_imp));
		const double x_init = 3.0 * radi_tar;
		double input = 0;
		std::cout << "Input L_init" << std::endl;
		std::cin >> input;
		const double L_init = L_EM * input;
		std::cout << "Input v_imp" << std::endl;
		std::cin >> input;
		const double v_imp = v_esc * input;

		const double v_inf = sqrt(std::max(v_imp * v_imp - v_esc * v_esc, 0.0));
		double y_init = radi_tar;//Initial guess.
		double v_init;
		std::cout << "v_esc = " << v_esc << std::endl;
		for(int it = 0 ; it < 10 ; ++ it){
			v_init = sqrt(v_inf * v_inf + 2.0 * Grav * mass_tar / sqrt(x_init * x_init + y_init * y_init));
			y_init = L_init / (mass_imp * v_init);
		}

		std::cout << "v_init = "  << v_init <<  std::endl;
		std::cout << "y_init / Rtar = "  << y_init / radi_tar <<  std::endl;
		std::cout << "v_imp  = "  << v_imp << " = " << v_imp / v_esc << "v_esc" << std::endl;
		std::cout << "m_imp  = " << mass_imp / M << std::endl;
		//shift'em
		for(PS::U32 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			if(sph_system[i].tag <= 2){
			}else{
				sph_system[i].pos   -= pos_imp;
				sph_system[i].vel   -= vel_imp;
				sph_system[i].pos.x += x_init;
				sph_system[i].pos.y += y_init;
				sph_system[i].vel.x -= v_init;
			}
		}
		//
		const double b = L_init / v_inf / mass_imp;
		const double a = - Grav * mass_tar / (v_inf * v_inf);
		const double rp = (sqrt(a * a + b * b) + a);
		const double r_imp = 2.0 / (v_imp * v_imp / (Grav * mass_tar) + 1.0 / a);
		std::cout << "a = " << a / radi_tar << " R_tar" << std::endl;
		std::cout << "b = " << b / radi_tar << " R_tar" << std::endl;
		std::cout << "r_imp = " << r_imp / radi_tar << " R_tar" << std::endl;
		std::cout << "r_p   = " << rp / radi_tar << " R_tar" << std::endl;
		std::cout << "angle = " << asin(r_imp / (radi_tar + radi_imp)) / M_PI << "pi" << std::endl;

		std::cout << "setup..." << std::endl;
	}
	static void postTimestepProcess(PS::ParticleSystem<Ptcl>& sph_system, system_t& sys){
		//SANITY Check
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			sph_system[i].eng  = std::max(sph_system[i].eng , 1.0e+4);
			sph_system[i].dens = std::max(sph_system[i].dens, 100.0);
		}
		//Shift Origin
		PS::F64vec com_loc = 0;//center of mass of target core
		PS::F64vec mom_loc = 0;//moment of target core
		PS::F64 mass_loc = 0;//mass of target core
		for(PS::S32 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			if(sph_system[i].tag != 0) continue;
			com_loc += sph_system[i].pos * sph_system[i].mass;
			mom_loc += sph_system[i].vel * sph_system[i].mass;
			mass_loc += sph_system[i].mass;
		}
		PS::F64vec com = PS::Comm::getSum(com_loc);
		PS::F64vec mom = PS::Comm::getSum(mom_loc);
		PS::F64 mass = PS::Comm::getSum(mass_loc);
		com /= mass;
		mom /= mass;
		for(PS::S32 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			sph_system[i].pos -= com;
			sph_system[i].vel -= mom;
		}
		#if 1
		std::size_t Nptcl = sph_system.getNumberOfParticleLocal();
		for(PS::S32 i = 0 ; i < Nptcl ; ++ i){
			if(sqrt(sph_system[i].pos * sph_system[i].pos) / R > 150.0){
				//bounded particles should not be killed.
				if(0.5 * sph_system[i].vel * sph_system[i].vel + sph_system[i].pot < 0) continue;
				std::cout << "KILL" << std::endl;
				sph_system[i] = sph_system[-- Nptcl];
				-- i;
			}
		}
		sph_system.setNumberOfParticleLocal(Nptcl);
		#endif
	}
	static void setEoS(PS::ParticleSystem<Ptcl>& sph_system){
		#pragma omp parallel for
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			if(sph_system[i].tag == 0 || sph_system[i].tag == 3){
				sph_system[i].setPressure(&Iron);
			}else if(sph_system[i].tag == 2){
				sph_system[i].setPressure(&Olivine);
			}else if(sph_system[i].tag == 1){
				sph_system[i].setPressure(&Dunite);
			}else{
				//sph_system[i].setPressure(&Basalt);
				sph_system[i].setPressure(&Dunite);
				//sph_system[i].setPressure(&Granite);
			}
		}
	}
};


