#define SELF_GRAVITY
#define FLAG_GI
#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
#error
#endif
#define GRAVITY
template <class Ptcl> class GI_init : public Problem<Ptcl>{
	public:
	static const double END_TIME = 10000.0;

	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		const PS::F64 UnitMass = 6.0e+24 * 0.9;
		const PS::F64 UnitRadi = 6400e+3 * 1.1;
		const PS::F64 Grav     = 6.67e-11;

		const PS::F64 CoreMass = UnitMass * 0.3;
		const PS::F64 MntlMass = (UnitMass - CoreMass);
		const PS::F64 CoreRadi = UnitRadi * 0.55;
		const PS::F64 CoreDens = CoreMass / (4.0 * M_PI / 3.0 * CoreRadi * CoreRadi * CoreRadi);
		const PS::F64 MntlDens = (UnitMass - CoreMass) / (4.0 * M_PI / 3.0 * (UnitRadi * UnitRadi * UnitRadi - CoreRadi * CoreRadi * CoreRadi));
		std::cout << CoreDens << " " << MntlDens << std::endl;
		//exit(0);

		if(PS::Comm::getRank() != 0) return;
		std::vector<Ptcl> ptcl;
		int Ncore = 0;
		int Nmntl = 0;
		{
			const int Nsurf = 3000;
			const double rout = 1.0;
			const double a = 4.0 * M_PI * rout * rout / (double)(Nsurf);
			const double d = sqrt(a);
			const int Nrad = round(rout / d);
			for(int l = 1 ; l <= Nrad ; ++ l){
				const double r = d * (l - 0.5);
				const int Ntheta = round(M_PI * r / d);
				const double dtheta = M_PI * r / Ntheta;
				const double dphi   = a / dtheta;
				for(int m = 0 ; m < Ntheta ; ++ m){
					const double theta = M_PI * (m + 0.5) / (double)(Ntheta);
					const int Nphi = std::abs(round(2.0 * M_PI * r * sin(theta) / dphi));
					for(int n = 0 ; n < Nphi ; ++ n){
						const double phi = 2.0 * M_PI * (double)(n) / (double)(Nphi);
						Ptcl ith;
						ith.pos.x = sin(theta) * cos(phi);
						ith.pos.y = sin(theta) * sin(phi);
						ith.pos.z = cos(theta);
						ith.pos *= r * CoreRadi;
						ith.dens = CoreDens;
						ith.mass = UnitMass;
						ith.eng = 0.01 * Grav * CoreMass / CoreRadi;
						ith.EoS = &Iron;
						ith.tag = 1;
						ptcl.push_back(ith);
						Ncore ++;
					}
				}
			}
		}
		{
			const int Nsurf = 5000;
			const double rout = 1.0;
			const double a = 4.0 * M_PI * rout * rout / (double)(Nsurf);
			const double d = sqrt(a);
			const int Nrad = round(rout / d);
			for(int l = 1 ; l <= Nrad ; ++ l){
				const double r = d * (l - 0.5);
				const int Ntheta = round(M_PI * r / d);
				const double dtheta = M_PI * r / Ntheta;
				const double dphi   = a / dtheta;
				for(int m = 0 ; m < Ntheta ; ++ m){
					const double theta = M_PI * (m + 0.5) / (double)(Ntheta);
					const int Nphi = std::abs(round(2.0 * M_PI * r * sin(theta) / dphi));
					for(int n = 0 ; n < Nphi ; ++ n){
						const double phi = 2.0 * M_PI * (double)(n) / (double)(Nphi);
						Ptcl ith;
						ith.pos.x = sin(theta) * cos(phi);
						ith.pos.y = sin(theta) * sin(phi);
						ith.pos.z = cos(theta);
						ith.pos *= r * UnitRadi;
						if(ith.pos * ith.pos < CoreRadi * CoreRadi) continue;
						ith.dens = MntlDens;
						ith.mass = UnitMass;
						ith.eng = 0.001 * Grav * MntlMass / UnitRadi;
						ith.EoS = &Olivine;
						ith.tag = 0;
						ptcl.push_back(ith);
					}
				}
			}
		}
		const PS::S32 numPtclLocal = ptcl.size();
		sph_system.setNumberOfParticleLocal(numPtclLocal);
		for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
			sph_system[i] = ptcl[i];
			sph_system[i].mass /= (double)(numPtclLocal);
		}
		//Fin.
		std::cout << "# of ptcls = " << ptcl.size() << ", core " << Ncore << std::endl;
		std::cout << "setup..." << std::endl;
	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo){
		if(sysinfo.time < 5000.0){
			std::cout << "Add Frictional Force." << std::endl;
			#pragma omp parallel for
			for(PS::S32 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
				sph_system[i].acc += - sph_system[i].vel * 0.05 / sph_system[i].dt;
			}
		}
	}

};

template <class Ptcl> class GI_init2 : public Problem<Ptcl>{
	public:
	static const double END_TIME = 10000.0;

	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		FileHeader header;
		char filename[256];
		sprintf(filename, "init");
		sph_system.readParticleAscii(filename, "init/%s_%05d_%05d.dat", header);
		#pragma omp parallel for
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			if(sph_system[i].tag % 2 == 1){
				sph_system[i].setPressure(&Iron);
			}else if(sph_system[i].tag == 0){
				sph_system[i].setPressure(&Olivine);
				//sph_system[i].setPressure(&Dunite);
			}else{
				sph_system[i].setPressure(&Dunite);
			}
		}
		
	}
	static void setEoS(PS::ParticleSystem<Ptcl>& sph_system){
		#pragma omp parallel for
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			if(sph_system[i].tag % 2 == 1){
				sph_system[i].setPressure(&Iron);
			}else if(sph_system[i].tag == 0){
				sph_system[i].setPressure(&Olivine);
				//sph_system[i].setPressure(&Dunite);
			}else{
				sph_system[i].setPressure(&Dunite);
			}
		}
	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo){
		if(sysinfo.time < 5000.0){
			std::cout << "Add Frictional Force." << std::endl;
			#pragma omp parallel for
			for(PS::S32 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
				sph_system[i].acc += - sph_system[i].vel * 0.1 / sph_system[i].dt;
				//sph_system[i].eng_dot += - sph_system[i].EoS->Temperature(sph_system[i].dens, sph_system[i].eng) * 1. / sph_system[i].dt;
			}
		}else{
			/*
			static bool isFirst = true;
			if(isFirst){
				std::cout << "eng" << std::endl;
				#pragma omp parallel for
				for(PS::S32 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
					const double r = sqrt(sph_system[i].pos * sph_system[i].pos);
					if(sph_system[i].tag % 2 == 1){
						sph_system[i].eng = sph_system[i].eng_half = Iron.getSpecificThermalEnergyViaDensityAndTemperature(sph_system[i].dens, 4000.0 * sqrt(-(r / 7.4e+6 - 1.0)) + 1500.0);
					}else if(sph_system[i].tag == 0){
						sph_system[i].eng = sph_system[i].eng_half = Olivine.getSpecificThermalEnergyViaDensityAndTemperature(sph_system[i].dens, 4000.0 * sqrt(-(r / 7.4e+6 - 1.0)) + 1500.0);
					}else{
						sph_system[i].eng = sph_system[i].eng_half = Dunite.getSpecificThermalEnergyViaDensityAndTemperature(sph_system[i].dens, 4000.0 * sqrt(-(r / 7.4e+6 - 1.0)) + 1500.0);
					}
					//if(isnan(sph_system[i].eng)) std::cout << "ERROR" << std::endl;
				}
				isFirst = false;
			}
			*/
		}
	}
};

