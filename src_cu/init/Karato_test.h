#pragma once


#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
#error
#else
template <class Ptcl> class TEST : public Problem<Ptcl>{
	public:
	static const double END_TIME = 0.25;
	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		/////////
		//place ptcls
		/////////
		std::vector<Ptcl> ptcl;
		const PS::F64 box_x = 10.0e+3;//10 km
		const PS::F64 box_y = box_x / 8.0;
		const PS::F64 box_z = box_x / 8.0;
		const PS::F64 dx = box_x / 64.0;
		const PS::F64 dens_sol = 3000.0;
		const PS::F64 dens_liq = 3000.0;

		PS::S32 i = 0;
		for(PS::F64 x = 0 ; x < box_x ; x += dx){
			for(PS::F64 y = 0 ; y < box_y ; y += dx){
				for(PS::F64 z = 0 ; z < box_z ; z += dx){
					Ptcl ith;
					ith.pos.x = x;
					ith.pos.y = y;
					ith.pos.z = z;
					if(x < box_x * 0.5){
						const PS::F64 temp = 1500.0;
						ith.dens = dens_sol;
						ith.eng  = Dunite.getSpecificThermalEnergyViaDensityAndTemperature(ith.dens, temp);
						ith.vel.x = 0.0;
						ith.tag = 0;
						//ith.setPressure(&Solid_);
						ith.setPressure(&Dunite);
					}else{
						const PS::F64 temp = 2000.0;
						ith.dens = dens_liq;
						ith.eng  = Olivine.getSpecificThermalEnergyViaDensityAndTemperature(ith.dens, temp);
						ith.vel.x = -10.0e+3;
						ith.tag = 1;
						//ith.setPressure(&Melt_);
						ith.setPressure(&Olivine);
					}
					ith.id   = i++;
					ptcl.push_back(ith);
				}
			}
		}
		for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
			ptcl[i].mass = ptcl[i].dens * box_x * box_y * box_z / (PS::F64)(ptcl.size());
		}
		std::cout << "# of ptcls is... " << ptcl.size() << std::endl;
		//
		dinfo.setBoundaryCondition(PS::BOUNDARY_CONDITION_PERIODIC_XYZ);
		dinfo.setPosRootDomain(PS::F64vec(0.0, 0.0, 0.0), PS::F64vec(box_x, box_y, box_z));
		if(PS::Comm::getRank() == 0){
			const PS::S32 numPtclLocal = ptcl.size();
			sph_system.setNumberOfParticleLocal(numPtclLocal);
			for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
				sph_system[i] = ptcl[i];
			}
		}else{
			sph_system.setNumberOfParticleLocal(0);
		}
		/////////
		//Fin.
		std::cout << "setup..." << std::endl;
	}
	static void setEoS(PS::ParticleSystem<Ptcl>& sph_system){
		#pragma omp parallel for
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			if(sph_system[i].tag == 0){
				//sph_system[i].setPressure(&Solid_);
				sph_system[i].setPressure(&Dunite);
			}else{
				//sph_system[i].setPressure(&Melt_);
				sph_system[i].setPressure(&Olivine);
			}
		}
	}
};
#endif

