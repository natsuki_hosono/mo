#define SELF_GRAVITY
#define FLAG_GI
#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
#error
#endif

template <class Ptcl> class GI_init : public Problem<Ptcl>{
	public:
	static const PS::F64 UnitMass = 6.0e+24; // Earth mass
	static const PS::F64 UnitRadi = 6400e+3 * 1.; // Earth radii
	static const PS::F64 Grav = 6.67e-11;
	
	static PS::F64 UnitTime(){
		return sqrt(UnitRadi * UnitRadi * UnitRadi / (Grav * UnitMass));
	}
	static PS::F64 DynamicalTime(){
		return math::pi * UnitTime();
	}
	static PS::F64 EscapeVelocity(){
		return sqrt(2.0 * Grav * UnitMass / UnitRadi);
	}
	static const double END_TIME = 1.0e+4;

	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		int putImpactor = 0;
		//const PS::F64 dx = 1.0 / 42.0;
		const PS::F64 dx = 1.0 / 17.0;
		//std::cin >> putImpactor;

		std::cout << "putImpactor = " << putImpactor << std::endl;
		/////////
		//place ptcls
		/////////
		std::vector<Ptcl> ptcl;
		std::vector<Ptcl> tar;//Target
		std::vector<Ptcl> imp;//Impactor
		/////////
		/////////
		const PS::F64 Expand = 1.1;
		const PS::F64 tarMass = UnitMass;
		const PS::F64 tarRadi = UnitRadi;
		const PS::F64 tarCoreMass = 0.3 * tarMass;
		const PS::F64 tarCoreRadi = cbrt(tarCoreMass / tarMass) * tarRadi;

		///////////////////
		//Dummy put to determine # of ptcls
		///////////////////
		//target
		int tarNmntl = 0;
		for(PS::F64 x = -1.0 ; x <= 1.0 ; x += dx){
			for(PS::F64 y = -1.0 ; y <= 1.0 ; y += dx){
				for(PS::F64 z = -1.0 ; z <= 1.0 ; z += dx){
					++ tarNmntl;
				}
			}
		}
		///////////////////
		//Dummy end
		///////////////////
		const int tarNptcl = tarNmntl;
		const int Nptcl    = tarNptcl;
		std::cout << "Target  :" << tarNptcl << std::endl;
		std::cout << "    radius           : " << tarRadi << std::endl;
		std::cout << "    # of mantle ptcls: " << tarNmntl << std::endl;
		std::cout << "    mantle density   : " << (tarMass) / (4.0 * math::pi / 3.0 * (tarRadi * tarRadi * tarRadi)) << std::endl;
		std::cout << "    core radius      : " << tarCoreRadi << std::endl;
		std::cout << "Total:" << Nptcl << std::endl;
		const int NptclIn1Node = Nptcl / PS::Comm::getNumberOfProc();
		///////////////////
		//Real put
		///////////////////
		PS::S32 id = 0;
		//Put Tar.
		for(PS::F64 x = -1.0 ; x <= 1.0 ; x += dx){
			for(PS::F64 y = -1.0 ; y <= 1.0 ; y += dx){
				for(PS::F64 z = -1.0 ; z <= 1.0 ; z += dx){
					const PS::F64 r = sqrt(x*x + y*y + z*z) * UnitRadi;
					//if(r >= tarRadi) continue;
					Ptcl ith;
					ith.pos.x = UnitRadi * x;
					ith.pos.y = UnitRadi * y;
					ith.pos.z = UnitRadi * z;
					ith.vel.x = EscapeVelocity() * 0.1 * (2.0 * (double)rand() / (double)RAND_MAX - 1.0);
					ith.vel.y = EscapeVelocity() * 0.1 * (2.0 * (double)rand() / (double)RAND_MAX - 1.0);
					ith.vel.z = EscapeVelocity() * 0.1 * (2.0 * (double)rand() / (double)RAND_MAX - 1.0);
					ith.dens = (tarMass) / (tarRadi * tarRadi * tarRadi);
					ith.mass = tarMass;
					ith.eng  = 0.01 * Grav * tarMass / tarRadi;
					ith.id   = id++;
					ith.setPressure(&Olivine);
					ith.tag = (sqrt(ith.pos * ith.pos) < tarCoreRadi) ? 1 : 0;
					if(ith.id % PS::Comm::getNumberOfProc() == PS::Comm::getRank()) tar.push_back(ith);
				}
			}
		}
		for(PS::U32 i = 0 ; i < tar.size() ; ++ i){
			tar[i].mass /= (PS::F64)(Nptcl);
		}

		for(PS::U32 i = 0 ; i < tar.size() ; ++ i){
			ptcl.push_back(tar[i]);
		}
		const PS::S32 numPtclLocal = ptcl.size();
		sph_system.setNumberOfParticleLocal(numPtclLocal);
		for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
			sph_system[i] = ptcl[i];
		}
		//Fin.
		std::cout << "# of ptcls = " << ptcl.size() << std::endl;
		std::cout << "setup..." << std::endl;
		dinfo.setBoundaryCondition(PS::BOUNDARY_CONDITION_PERIODIC_XYZ);
		dinfo.setPosRootDomain(PS::F64vec(-UnitRadi, -UnitRadi, -UnitRadi), PS::F64vec(UnitRadi, UnitRadi, UnitRadi));
	}
	static void setEoS(PS::ParticleSystem<Ptcl>& sph_system){
		#pragma omp parallel for
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			if(sph_system[i].tag >= 2){
				sph_system[i].setPressure(&Dunite);
			}else{
				sph_system[i].setPressure(&Olivine);
			}
		}
	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo){
		if(sysinfo.time < 5000.0){
			std::cout << "Add Frictional Force." << std::endl;
			#pragma omp parallel for
			for(PS::S32 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
				sph_system[i].acc += - sph_system[i].vel * 0.1 / sph_system[i].dt;
			}
		}
	}
};

template <class Ptcl> class GI_init2 : public Problem<Ptcl>{
	public:
	static const double END_TIME = 1.0e+4;
	static const PS::F64 R = 6400.0e+3;
	static const double Grav = 6.67e-11;
	static const double L_EM = 3.5e+34;
	//static const PS::F64 AngMomEarthSpin = 7.1e+33;???
	static const PS::F64 InertiaEarth    = 8.1e+37;

	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		FileHeader header;
		char filename[256];
		sprintf(filename, "init");
		//std::cout << "TEST" << std::endl;
		sph_system.readParticleAscii(filename, "%s_%05d_%05d.dat", header);
		#pragma omp parallel for
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			if(sph_system[i].tag >= 2){
				sph_system[i].setPressure(&Dunite);
			}else{
				sph_system[i].setPressure(&Olivine);
			}
		}
	}
	static void setEoS(PS::ParticleSystem<Ptcl>& sph_system){
		#pragma omp parallel for
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			if(sph_system[i].tag >= 2){
				sph_system[i].setPressure(&Dunite);
			}else{
				sph_system[i].setPressure(&Olivine);
			}
		}
	}
};

