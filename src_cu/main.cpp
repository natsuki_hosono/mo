#include <particle_simulator.hpp>

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>

#include "param.h"
#include "mathfunc.h"
#include "kernel.h"
#include "EoS.h"
#include "class.h"
#include "force.h"
//=============
#include "init/GI.h"
//#include "init/GI2.h"
//#include "init/sphere.h"
//#include "init/Karato_test.h"
//=============
#include "test.h"
#include "io.h"
#include "integral.h"

#ifdef ENABLE_GPU
#include "kernel.cuh"
#endif

int main(int argc, char* argv[]){
	namespace PTCL = STD;
	//typedef TEST<PTCL::RealPtcl> PROBLEM;
	//typedef KHI<PTCL::RealPtcl> PROBLEM;
	//typedef RTI<PTCL::RealPtcl> PROBLEM;
	typedef GI_init<PTCL::RealPtcl> PROBLEM;
	//typedef GI_init2<PTCL::RealPtcl> PROBLEM;
	//typedef GI<PTCL::RealPtcl> PROBLEM;
	//////////////////
	//Create vars.
	//////////////////
	PS::Initialize(argc, argv);
	PS::ParticleSystem<PTCL::RealPtcl> sph_system;
	sph_system.initialize();
	PS::DomainInfo dinfo;
	dinfo.initialize();
	system_t sysinfo;

	sph_system.setAverageTargetNumberOfSampleParticlePerProcess(200);
	//////////////////
	//Setup Initial
	//////////////////
	if(argc == 1){
		PROBLEM::setupIC(sph_system, sysinfo, dinfo);
		PROBLEM::setEoS(sph_system);
		PTCL::CalcPressure(sph_system);
	}else{
		#if 0
		sysinfo.step = atoi(argv[1]);
		InputFileWithTimeInterval<PTCL::RealPtcl>(sph_system, sysinfo);
		//DebugInputFile<PTCL::RealPtcl>(sph_system, sysinfo);
		PROBLEM::setEoS(sph_system);
		#else
		InputBinary<PTCL::RealPtcl>(sph_system, sysinfo);
		PROBLEM::setEoS(sph_system);
		#endif
	}
	PROBLEM::setDomain(dinfo);

	#pragma omp parallel for
	for(PS::S32 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
		sph_system[i].initialize();
	}

	//Dom. info
	dinfo.decomposeDomainAll(sph_system);
	sph_system.exchangeParticle(dinfo);
	//plant tree
	PS::TreeForForceShort<PTCL::RESULT::Dens, PTCL::EPI::Dens, PTCL::EPJ::Dens>::Gather   dens_tree;
	PS::TreeForForceShort<PTCL::RESULT::Drvt, PTCL::EPI::Drvt, PTCL::EPJ::Drvt>::Gather   drvt_tree;
	PS::TreeForForceShort<PTCL::RESULT::Hydr, PTCL::EPI::Hydr, PTCL::EPJ::Hydr>::Symmetry hydr_tree;
	#ifdef SELF_GRAVITY
	PS::TreeForForceLong <PTCL::RESULT::Grav, PTCL::EPI::Grav, PTCL::EPJ::Grav>::Monopole grav_tree;
	#endif
	
	dens_tree.initialize(sph_system.getNumberOfParticleLocal(), 0.5, PARAM::NUMBER_OF_LEAF_LIMIT, PARAM::NUMBER_OF_GROUP_LIMIT);
	drvt_tree.initialize(sph_system.getNumberOfParticleLocal(), 0.5, PARAM::NUMBER_OF_LEAF_LIMIT, PARAM::NUMBER_OF_GROUP_LIMIT);
	hydr_tree.initialize(sph_system.getNumberOfParticleLocal(), 0.5, PARAM::NUMBER_OF_LEAF_LIMIT, PARAM::NUMBER_OF_GROUP_LIMIT);
	#ifdef SELF_GRAVITY
	grav_tree.initialize(sph_system.getNumberOfParticleLocal(), 0.5, 8, 64);
	#endif
	for(short int loop = 0 ; loop <= PARAM::NUMBER_OF_DENSITY_SMOOTHING_LENGTH_LOOP ; ++ loop){
		const double start = PS::GetWtime();
		#ifdef ENABLE_GPU
			dens_tree.calcForceAllAndWriteBackMultiWalk(DENS::DispatchKernelDynamicAllocate, DENS::RetrieveKernelDynamicAllocate, 1, sph_system, dinfo, N_WALK_LIMIT, true);
		#else
			dens_tree.calcForceAllAndWriteBack(PTCL::CalcDensity(), sph_system, dinfo);
		#endif
	}
	std::cout << "thds::" << PS::Comm::getNumberOfThread() << std::endl;

	PTCL::CalcPressure(sph_system);
	#ifdef ENABLE_GPU
		drvt_tree.calcForceAllAndWriteBackMultiWalk(DrvtDispatchKernel, DrvtRetrieveKernel, 1, sph_system, dinfo, N_WALK_LIMIT, true);
	#else
		drvt_tree.calcForceAllAndWriteBack(PTCL::CalcDerivative(), sph_system, dinfo);
	#endif

	#ifdef ENABLE_GPU
		hydr_tree.calcForceAllAndWriteBackMultiWalk(HydrDispatchKernel, HydrRetrieveKernel, 1, sph_system, dinfo, N_WALK_LIMIT, true);
	#else
		hydr_tree.calcForceAllAndWriteBack(PTCL::CalcHydroForce(), sph_system, dinfo);
	#endif

	#ifdef SELF_GRAVITY
	grav_tree.calcForceAllAndWriteBack(PTCL::CalcGravityForce<PTCL::EPJ::Grav>(), PTCL::CalcGravityForce<PS::SPJMonopole>(), sph_system, dinfo);
	#endif
	PROBLEM::addExternalForce(sph_system, sysinfo);
	OutputFileWithTimeInterval(sph_system, sysinfo, PROBLEM::END_TIME);

	//std::cout << "get dt" << std::endl;
	sysinfo.dt = getTimeStepGlobal<PTCL::RealPtcl>(sph_system);

	/////////////
	if(PS::Comm::getRank() == 0){
		std::cout << "//================================" << std::endl;
		std::cout << std::scientific << std::setprecision(16) << "time = " << sysinfo.time << ", dt = " << sysinfo.dt << std::endl;
		std::cout << "step = " << sysinfo.step << std::endl;
		std::cout << "//================================" << std::endl;
	}
	/////////////

	while(sysinfo.time < PROBLEM::END_TIME){
		//std::cout << "integ" << std::endl;
		#pragma omp parallel for
		for(int i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			sph_system[i].initialKick(sysinfo.dt);
			sph_system[i].fullDrift(sysinfo.dt);
			sph_system[i].predict(sysinfo.dt);
		}
		sysinfo.time += sysinfo.dt;
		//std::cout << "adjust" << std::endl;
		sph_system.adjustPositionIntoRootDomain(dinfo);
		//std::cout << "decompose" << std::endl;
		dinfo.decomposeDomainAll(sph_system);
		//std::cout << "exchange" << std::endl;
		sph_system.exchangeParticle(dinfo);
		PROBLEM::setEoS(sph_system);
		{
			const double start = PS::GetWtime();
			for(short int loop = 0 ; loop < PARAM::NUMBER_OF_DENSITY_SMOOTHING_LENGTH_LOOP ; ++ loop){
				#ifdef ENABLE_GPU
					//dens_tree.calcForceAllAndWriteBackMultiWalk(DENS::DispatchKernel, DENS::RetrieveKernel, 1, sph_system, dinfo, N_WALK_LIMIT, true);
					dens_tree.calcForceAllAndWriteBackMultiWalk(DENS::DispatchKernelDynamicAllocate, DENS::RetrieveKernelDynamicAllocate, 1, sph_system, dinfo, N_WALK_LIMIT, true);
				#else
					dens_tree.calcForceAllAndWriteBack(PTCL::CalcDensity(), sph_system, dinfo);
				#endif
			}
			std::cout << "Dens:" << PS::GetWtime() - start << std::endl;
		}
		PTCL::CalcPressure(sph_system);
		{
			const double start = PS::GetWtime();
			#ifdef ENABLE_GPU
			drvt_tree.calcForceAllAndWriteBackMultiWalk(DrvtDispatchKernel, DrvtRetrieveKernel, 1, sph_system, dinfo, N_WALK_LIMIT, true);
			#else
			drvt_tree.calcForceAllAndWriteBack(PTCL::CalcDerivative(), sph_system, dinfo);
			#endif
			std::cout << "Drvt:" << PS::GetWtime() - start << std::endl;
		}
		{
			const double start = PS::GetWtime();
			#ifdef ENABLE_GPU
			hydr_tree.calcForceAllAndWriteBackMultiWalk(HydrDispatchKernel, HydrRetrieveKernel, 1, sph_system, dinfo, N_WALK_LIMIT, true);
			#else
			hydr_tree.calcForceAllAndWriteBack(PTCL::CalcHydroForce(), sph_system, dinfo);
			#endif
			std::cout << "Hydr:" << PS::GetWtime() - start << std::endl;
		}

		#ifdef SELF_GRAVITY
		//std::cout << "Calc Grav" << std::endl;
		const double start = PS::GetWtime();
		grav_tree.calcForceAllAndWriteBack(PTCL::CalcGravityForce<PTCL::EPJ::Grav>(), PTCL::CalcGravityForce<PS::SPJMonopole>(), sph_system, dinfo);
		std::cout << "Grav:" << PS::GetWtime() - start << std::endl;
		#endif
		PROBLEM::addExternalForce(sph_system, sysinfo);
		#pragma omp parallel for
		for(int i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			sph_system[i].finalKick(sysinfo.dt);
		}
		PROBLEM::postTimestepProcess(sph_system, sysinfo);
		sysinfo.dt = getTimeStepGlobal<PTCL::RealPtcl>(sph_system);
		OutputFileWithTimeInterval<PTCL::RealPtcl>(sph_system, sysinfo, PROBLEM::END_TIME);
		++ sysinfo.step;
		if(PS::Comm::getRank() == 0){
			std::cout << "//================================" << std::endl;
			std::cout << std::scientific << std::setprecision(16) << "time = " << sysinfo.time << ", dt = " << sysinfo.dt << std::endl;
			std::cout << "step = " << sysinfo.step << std::endl;
			std::cout << "//================================" << std::endl;
		}
		if(sysinfo.step % 30 == 0){
			DebugOutputFile(sph_system, sysinfo);
			OutputBinary(sph_system, sysinfo);
		}
	}

	PS::Finalize();
	return 0;
}

