import sys
import math
import numpy as np

#import matplotlib as mpl
#import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D
#fig = plt.figure()
#box_size = 4.0

Nnode = 32

G = 6.67e-11
R = 6400e+3
M = 6.0e+24


for s in range(174, 175):
	sys.stderr.write(str(s) + "\n")
	tag = []
	mass = []
	rx = []
	ry = []
	rz = []
	vx = []
	vy = []
	vz = []
	pot = []

	disk_mass = [0, 0, 0, 0, 0]
	disk_AM   = [0, 0, 0, 0, 0]

	for i in range(Nnode):
		data = np.loadtxt("result/{0:0>5}_{1:0>5}_{2:0>5}.dat".format(s, Nnode, i), skiprows=2, delimiter="\t");
		tag.extend(data[:, 1])
		mass.extend(data[:, 2])
		rx.extend(data[:, 3])
		ry.extend(data[:, 4])
		rz.extend(data[:, 5])
		vx.extend(data[:, 6])
		vy.extend(data[:, 7])
		vz.extend(data[:, 8])
		pot.extend(data[:, 12])

	for i in range(len(rx)):
		if(0.5 * (vx[i] * vx[i] + vy[i] * vy[i] + vz[i] * vz[i]) + pot[i] < 0):
			if(rx[i] * vy[i] - ry[i] * vx[i] > math.sqrt(G * M * R)):
				disk_mass[int(tag[i])] += mass[i]
				disk_AM[int(tag[i])] += (rx[i] * vy[i] - ry[i] * vx[i]) * mass[i]
		rx[i] /= R
		ry[i] /= R
		rz[i] /= R

	disk_mass_tot = 0
	disk_AM_tot = 0

	#ax = Axes3D(fig)
	for i in range(5):
		disk_mass_tot += disk_mass[i]
		disk_AM_tot += disk_AM[i]

	#ax.scatter(rx, ry, rz, c=tag, alpha=0.1, cmap='Blues')
	#plt.savefig("./img/%05d.png" % s)

	moon_mass = 1.9 * disk_AM_tot / math.sqrt(G * M * 2.9 * R) - (1.1 - 1.9 * 0.05) * disk_mass_tot

	print(str(moon_mass / (0.012 * M)) + " " + str(disk_mass_tot / (0.012 * M)) + " " + str((disk_mass[0] + disk_mass[1] + disk_mass[2]) / disk_mass_tot) + " " + str(disk_mass[2] / disk_mass_tot) + " " + str((disk_mass[0] + disk_mass[3])/disk_mass_tot))
