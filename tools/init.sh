#!/bin/sh -x

#PJM -L node=32
#PJM -L elapse=24:00:00
#PJM -j

export OMP_NUM_THREADS=32
export PARALLEL=32

mpirun /work/system/bin/msh "mkdir ./result"
#mpirun -of-proc ./stdout -stdin ./param.txt ./sph.out 21
mpirun -of-proc ./stdout ./sph.out 

