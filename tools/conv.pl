use strict;

my $Nnode = 32;

for(my $i = 0 ; $i < $Nnode ; $i += 1){
	print "$i\n";
	my (@tar, @imp);
	open (TAR, sprintf("../tar/step1_DI/result/00200_%05d_%05d.dat", $Nnode, $i));
	@tar = <TAR>;
	close TAR;
	open (IMP, sprintf("../../imp/step1_DI/result/00200_%05d_%05d.dat", $Nnode, $i));
	@imp = <IMP>;
	close IMP;
	my $Nptcl = $tar[1] + $imp[1];
	print $Nptcl . "\n";
	open (OUT, sprintf(">init/init_%05d_%05d.dat", $Nnode, $i));
	print OUT "$tar[0]";
	print OUT "$Nptcl\n";
	for(my $ptcl = 2 ; $ptcl <= $#tar ; $ptcl +=1){
		print OUT $tar[$ptcl];
	}
	for(my $ptcl = 2 ; $ptcl <= $#imp ; $ptcl +=1){
		print OUT $imp[$ptcl];
	}
	close OUT;
}

