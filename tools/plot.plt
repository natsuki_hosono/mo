#set term pngcairo size 512, 512
set term png size 512, 512
set out sprintf("img/%05d.png", i)

R = 6400e+3
G = 6.67e-11
M = 6.0e+24

set xrange[-15:15]
set yrange[-15:15]
set size ratio -1

unset colorbox
set palette defined(0 "grey", 1 "orange", 2 "red", 3 "black", 4 "blue")

unset xtics
unset ytics

#p sprintf("< cat result/%05d_*.dat | sort -n -k 7", i) u ($4/R):($5/R):2 pal pt 7 ps 0.25
set multiplot layout 2, 2
set tmargin 0.0
set lmargin 0.0
set rmargin 0.0
set bmargin 0.0
set cbrange[0:4]
unset key

disc_mass = system("sed -n " . (i + 1) . "p ./Moon.txt | cut -f 2 -d ' ' ") * 1.0
targ_frac = system("sed -n " . (i + 1) . "p ./Moon.txt | cut -f 3 -d ' ' ") * 1.0
MO_frac   = system("sed -n " . (i + 1) . "p ./Moon.txt | cut -f 4 -d ' ' ") * 1.0
iron_frac = system("sed -n " . (i + 1) . "p ./Moon.txt | cut -f 5 -d ' ' ") * 1.0

set label 1 sprintf("%.02f M_L\nTarget %.02f\nMO %.02f\nFe %.02f", disc_mass, targ_frac, MO_frac, iron_frac) at graph 0.01, graph 0.9 font ", 36pt" front

p sprintf("< cat result/%05d_*.dat", i) u ($4/R):($5/R):2 pal w d
unset label 1

p sprintf("< cat result/%05d_*.dat", i) u ($4/R):(0.5 * ($7**2 + $8**2 + $9**2) + $13 < 0 && $4 * $8 - $5 * $7 > sqrt(G * M * R) ? $5/R : NaN):2 pal w d
p sprintf("< cat result/%05d_*.dat", i) u ($4/R):(0.5 * ($7**2 + $8**2 + $9**2) + $13 < 0 && $4 * $8 - $5 * $7 < sqrt(G * M * R) ? $5/R : NaN):2 pal w d
p sprintf("< cat result/%05d_*.dat", i) u ($4/R):(0.5 * ($7**2 + $8**2 + $9**2) + $13 > 0 ? $5/R : NaN):2 pal w d

unset multiplot

